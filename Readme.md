# Music C++

簡單的音樂播放

Usage
---------------------

* 打開terminal，安裝套件
```
sudo apt-get install libao-dev libmpg123-dev 
```
    
* clone程式檔    
    
* 編譯程式    
```
gcc -O2 -o play test1.c -lmpg123 -lao
```
    
* 執行    
```
./play countingStar.mp3
```
